/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oesteretail.dao;

import com.oesteretail.model.Producto;
import java.util.List;

/**
 *
 * @author Jeremy
 */
public interface ProductoDAO {
    /**
	 * @param productoId
	 * @return producto
	 */
	public abstract Producto getProductoById(int productoId);

	/**
	 * @return producto list
	 */
	public abstract List<Producto> getProductoList();

	/**
	 * @param producto
	 * @return boolean added
	 */
	public abstract boolean addProducto(Producto producto);

	/**
	 * @param producto
	 * @param productoId
	 * @return product updated
	 */
	public abstract Producto updateProducto(Producto producto, int productoId);

	/**
	 * @param productoId
	 * @return boolean deleted
	 */
	public abstract boolean removeProducto(int productoId);
    
}
