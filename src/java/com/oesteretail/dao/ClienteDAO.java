/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oesteretail.dao;

import com.oesteretail.model.Cliente;
import java.sql.SQLException;

/**
 *
 * @author Jeremy
 */
public interface ClienteDAO {
    /**
	 * @param user
	 * @param clave
	 * @return Cliente
	 */
	public abstract Cliente getClienteByUserAndClave(String user, String clave);

	/**
	 * @param clienteId
	 * @return cliente
	 */
	public abstract Cliente getClienteById(int clienteId);

	/**
	 * @param user
	 * @return cliente
	 */
	public abstract Cliente getClienteByUser(String user);

	/**
	 * @param clienteId
	 * @param productoId
	 * @param cliente
	 * @param producto
	 * @return boolean
	 */
	public abstract boolean addPedido(int clienteId, int productoId);

	/**
	 * @param clienteId
	 * @param productoId
	 * @return boolean
	 */
	public abstract boolean removePedido(int clienteId, int productoId);
        
        
        public boolean isValidUser(String user, String clave) throws SQLException;
}


