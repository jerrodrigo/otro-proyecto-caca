/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oesteretail.controller;
import java.security.Principal;
import javax.swing.JOptionPane;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.servlet.ModelAndView;


/**
 *
 * @author wilson
 */
public class LoginController {


    @RequestMapping("/index.htm")
    public ModelAndView hello(ModelMap model, Principal principal) {
        JOptionPane.showMessageDialog(null, "llego al index");
        String loggedInUserName = principal.getName();
        
        return new ModelAndView("index", "user", loggedInUserName);
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(ModelMap model) {
       
        return "login";

    }

    @RequestMapping(value = "/login.htm?error", method = RequestMethod.GET)
    public String loginError(ModelMap model) {
        JOptionPane.showMessageDialog(null, "llego al error");
        model.addAttribute("error", "true");
        return "login";

    }

 

}
