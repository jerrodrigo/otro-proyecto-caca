/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oesteretail.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jeremy
 */
@Entity
@Table(name = "producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p")
    , @NamedQuery(name = "Producto.findByProductoId", query = "SELECT p FROM Producto p WHERE p.productoId = :productoId")
    , @NamedQuery(name = "Producto.findByProductoNombre", query = "SELECT p FROM Producto p WHERE p.productoNombre = :productoNombre")
    , @NamedQuery(name = "Producto.findByProductoMarca", query = "SELECT p FROM Producto p WHERE p.productoMarca = :productoMarca")
    , @NamedQuery(name = "Producto.findByProductoModelo", query = "SELECT p FROM Producto p WHERE p.productoModelo = :productoModelo")
    , @NamedQuery(name = "Producto.findByProductoPrecio", query = "SELECT p FROM Producto p WHERE p.productoPrecio = :productoPrecio")
    , @NamedQuery(name = "Producto.findByProductoStock", query = "SELECT p FROM Producto p WHERE p.productoStock = :productoStock")
    , @NamedQuery(name = "Producto.findByProductoImg", query = "SELECT p FROM Producto p WHERE p.productoImg = :productoImg")
    , @NamedQuery(name = "Producto.findByCategoriaId", query = "SELECT p FROM Producto p WHERE p.categoriaId = :categoriaId")})
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "productoId")
    private Integer productoId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "productoNombre")
    private String productoNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "productoMarca")
    private String productoMarca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "productoModelo")
    private String productoModelo;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "productoDescripcion")
    private String productoDescripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "productoPrecio")
    private double productoPrecio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "productoStock")
    private int productoStock;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "productoImg")
    private String productoImg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "categoriaId")
    private int categoriaId;

    public Producto() {
    }

    public Producto(Integer productoId) {
        this.productoId = productoId;
    }

    public Producto(Integer productoId, String productoNombre, String productoMarca, String productoModelo, String productoDescripcion, double productoPrecio, int productoStock, String productoImg, int categoriaId) {
        this.productoId = productoId;
        this.productoNombre = productoNombre;
        this.productoMarca = productoMarca;
        this.productoModelo = productoModelo;
        this.productoDescripcion = productoDescripcion;
        this.productoPrecio = productoPrecio;
        this.productoStock = productoStock;
        this.productoImg = productoImg;
        this.categoriaId = categoriaId;
    }

    public Integer getProductoId() {
        return productoId;
    }

    public void setProductoId(Integer productoId) {
        this.productoId = productoId;
    }

    public String getProductoNombre() {
        return productoNombre;
    }

    public void setProductoNombre(String productoNombre) {
        this.productoNombre = productoNombre;
    }

    public String getProductoMarca() {
        return productoMarca;
    }

    public void setProductoMarca(String productoMarca) {
        this.productoMarca = productoMarca;
    }

    public String getProductoModelo() {
        return productoModelo;
    }

    public void setProductoModelo(String productoModelo) {
        this.productoModelo = productoModelo;
    }

    public String getProductoDescripcion() {
        return productoDescripcion;
    }

    public void setProductoDescripcion(String productoDescripcion) {
        this.productoDescripcion = productoDescripcion;
    }

    public double getProductoPrecio() {
        return productoPrecio;
    }

    public void setProductoPrecio(double productoPrecio) {
        this.productoPrecio = productoPrecio;
    }

    public int getProductoStock() {
        return productoStock;
    }

    public void setProductoStock(int productoStock) {
        this.productoStock = productoStock;
    }

    public String getProductoImg() {
        return productoImg;
    }

    public void setProductoImg(String productoImg) {
        this.productoImg = productoImg;
    }

    public int getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(int categoriaId) {
        this.categoriaId = categoriaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoId != null ? productoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.productoId == null && other.productoId != null) || (this.productoId != null && !this.productoId.equals(other.productoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.oesteretail.model.Producto[ productoId=" + productoId + " ]";
    }
    
}
