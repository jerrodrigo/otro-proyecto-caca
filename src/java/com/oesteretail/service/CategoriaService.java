package com.oesteretail.service;

import com.oesteretail.model.Categoria;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoriaService {
    
    @PersistenceContext
    private EntityManager em;
    
    
    public List<Categoria> getAll(){
        
        List<Categoria> categorias = em.createNamedQuery("Categoria.findAll", Categoria.class).getResultList();
        
        return categorias;
    }
    
    public Categoria getByCodigo(int categoriaId){
        
        Categoria categoria = null;
        
        String query = "SELECT c FROM Categoria c WHERE c.categoriaId = :categoriaId";
        
        try{
            categoria = em.createQuery(query, Categoria.class)
                                .setParameter("categoriaId", categoriaId)
                                .getSingleResult();
        }
        catch(NoResultException ex){
            //Para cuando no hay resultados...
            categoria = null;
        }
        
        return categoria;
    }
    
    @Transactional(rollbackFor = ExceptionService.class)
    public void add(Categoria cat) throws ExceptionService{                
        
        Categoria categoria = getByCodigo(cat.getCategoriaId());
        
        if(categoria != null)
            throw new ExceptionService("No se puede repetir el código de la categoría");
        else
            em.persist(cat);
        
    }
    
    /*
    @Transactional
    public void add(Categoria cat){
        em.persist(cat);
    }
    */
    
}
