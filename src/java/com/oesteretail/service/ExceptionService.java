package com.oesteretail.service;

public class ExceptionService extends Exception {
    
    public ExceptionService(String msg){
        super(msg);
    }
    
}
