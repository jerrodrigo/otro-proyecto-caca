<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>        
        
        <a href="ver-categorias.htm">Ver Categorías</a>
        <br/>
        <a href="crear-categoria.htm">Crear Categoría</a>        
        
        <table border="1" style="margin-top: 20px;">
            <tr>
                <td>Código</td>
                <td>Nombre</td>
            </tr>
            <c:forEach var="cat" items="${attCategorias}">
                <tr>
                    <td>${cat.categoriaId}</td>
                    <td>${cat.categoriaNombre}</td>
                </tr>
            </c:forEach>
        </table>
        <br/>        
        ${attMensaje}        
    </body>
</html>
